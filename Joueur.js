var Bullet = require('./Bullet.js');
module.exports = function Joueur(name, color) {
    this.name = name;
    this.color = color;
    this.posX = 400;
    this.posY = 300;
    this.mousePosX = 400;
    this.mousePosY = 300;
    this.vx = 0;
    this.vy = 0;
    this.score = 0;
    this.pedobearsKilled = 0;
    this.vivant = false;
    this.currentSpeed = 0;
    this.maxSpeed = 2;
    this.listOfBullets = [];
    this.update = function(inputStates) {
        this.vx = this.vy = 0;
        if (inputStates.left && this.posX > -8+24) {
            this.vx = - this.maxSpeed;
        }
        if (inputStates.up && this.posY > 16) {
            this.vy = - this.maxSpeed;
        }
        if (inputStates.right  && this.posX < 800-16) {
            this.vx = this.maxSpeed;
        }
        if (inputStates.down && this.posY < 600-16) {
            this.vy = this.maxSpeed;
        }
        this.posX += this.vx;
        this.posY += this.vy;
    };
    this.fire =  function () {
        var dx = this.mousePosX - this.posX;
        var dy = this.mousePosY - this.posY;
        var theta = Math.atan(dy / dx);
        var reverse = 1;
        if (dx < 0) {
            reverse = -1;
        }
        var bul = new Bullet(this.posX, this.posY, theta, reverse);
        this.listOfBullets.push(bul);
        //socket.emit('sendnewbullet', this);
    };
};