/**
 * Created by Zizou on 19/02/2015.
 */

module.exports = function PedoBear() {
    this.currentSpeed = 0;
    this.maxSpeed = 2;
    var rand = Math.floor(Math.random() * 4);
    if(rand == 0){
        this.posX = Math.floor(Math.random() * 801);
        this.posY = 0;
        this.vx = 0;
        this.vy = 1;
        this.maxSpeed = Math.floor(Math.random() * 5) + 3;
    } else if(rand == 1){
        this.posX = 0;
        this.posY = Math.floor(Math.random() * 601);
        this.vx = 1;
        this.vy = 0;
        this.maxSpeed = Math.floor(Math.random() * 5) + 3;
    } else if(rand == 2){
        this.posX = Math.floor(Math.random() * 801);
        this.posY = 600;
        this.vx = 0;
        this.vy = -1;
        this.maxSpeed = Math.floor(Math.random() * 5) + 3;
    } else if(rand == 3){
        this.posX = 800;
        this.posY =Math.floor(Math.random() * 601);
        this.vx = -1;
        this.vy = 0;
        this.maxSpeed = Math.floor(Math.random() * 5) + 3;
    }
    this.update = function(allPlayers) {
        var ShortestdistX;
        var ShortestdistY;
        var tempDistX;
        var tempDistY;
        var playerName;
        ShortestdistX = 1000;
        ShortestdistY = 1000;
        for(player in allPlayers){
            if(!allPlayers[player].vivant) continue;
            tempDistX = Math.abs(allPlayers[player].posX - 10 -  this.posX);
            tempDistY = Math.abs(allPlayers[player].posY - 20 -  this.posY);
            if((tempDistX + tempDistY) < (ShortestdistX + ShortestdistY)){
                ShortestdistX = tempDistX;
                ShortestdistY = tempDistY;
                playerName = allPlayers[player].name;
            }
        }
        if(allPlayers[playerName] == undefined) return;
        var newVx = (allPlayers[playerName].posX - 10 -  this.posX) / 800 * this.maxSpeed;
        var newVy = (allPlayers[playerName].posY - 20 -  this.posY) / 600 * this.maxSpeed;
        var bestValue = 0;
        if(newVx < 0){
            bestValue -= Math.max(-newVx, 0.8);
            this.vx = bestValue;
        } else{
            this.vx = Math.max(newVx, 0.8);
        }
        if(newVy < 0){
            bestValue -= Math.max(-newVy, 0.8);
            this.vy = bestValue;
        } else{
            this.vy = Math.max(newVy, 0.8);
        }
        this.posX += this.vx / Object.keys(allPlayers).length;
        this.posY += this.vy / Object.keys(allPlayers).length;
    };
};