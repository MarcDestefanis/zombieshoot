module.exports = function Bullet(x, y, angle, reverse){
    /**
     * Bounds for the bullet
     */
    this.maxX = 800;
    this.maxY = 600;
    this.x = x;
    this.y = y;
    this.reverse = reverse;
    this.shallRemove = false;
    this.vel = {
        x: this.reverse * 25 * Math.cos(angle),
        y: this.reverse * 25 * Math.sin(angle)
    };
    this.update = function() {
        // saves previous position, used when rendering
        this.prevx = this.x;
        this.prevy = this.y;

        // inside bounds check
        if (-25 > this.x || this.x > this.maxX + 25||
            -25 > this.y || this.y > this.maxY + 25
        ) {
            this.shallRemove = true;
        }
        // translate position
        this.x += this.vel.x;
        this.y += this.vel.y;
    };
};