// SERVER \\

var Joueur = require('./Joueur.js');
var PedoBear = require('./PedoBear.js');

// We need to use the express framework: have a real web servler that knows how to send mime types etc.
var express=require('express');

// Init globals variables for each module required
var app = express()
    , http = require('http')
    , server = http.createServer(app)
    , io = require('socket.io').listen(server);

// launch the http server on given port
server.listen(8080);

// Indicate where static files are located. Without this, no external js file, no css...  
app.use(express.static(__dirname + '/'));

// routing
app.get('/', function (req, res) {
    res.sendfile(__dirname + '/PedobearsAttack.html');
});

var listOfPlayers = {};
var listOfPedoBears = [];

// Collisions between rectangle and circle
function circRectsOverlap(x0, y0, w0, h0, cx, cy, r) {
    var testX=cx;
    var testY=cy;

    if (testX < x0) testX=x0;
    if (testX > (x0+w0)) testX=(x0+w0);
    if (testY < y0) testY=y0;
    if (testY > (y0+h0)) testY=(y0+h0);

    return (((cx-testX)*(cx-testX)+(cy-testY)*(cy-testY))<r*r);
}

io.sockets.on('connection', function (socket) {
    // when the client emits 'sendnewmousepos', this listens and executes
    socket.on('sendnewmousepos', function (data) {
        listOfPlayers[data.user].mousePosX = data.mousePos.x;
        listOfPlayers[data.user].mousePosY = data.mousePos.y;
        io.sockets.emit('updatemousepos', data);
    });

    socket.on('createBullet', function(){
        listOfPlayers[socket.username].fire();
    });

    socket.on('updateBullet', function(){
        for (var i = 0, len = listOfPlayers[socket.username].listOfBullets.length; i < len; i++) {
            var b = listOfPlayers[socket.username].listOfBullets[i];
            b.update();
            // remove bullet if removeflag is setted
            if (b.shallRemove) {
                listOfPlayers[socket.username].listOfBullets.splice(i, 1);
                len--;
                i--;
                continue;
            }
            for(var pedoBear in listOfPedoBears){
                if(circRectsOverlap(
                        listOfPedoBears[pedoBear].posX,
                        listOfPedoBears[pedoBear].posY,
                        20,
                        40,
                        b.x,
                        b.y,
                        2)){
                    listOfPlayers[socket.username].score += 100;
                    listOfPlayers[socket.username].pedobearsKilled++;
                    io.sockets.emit('updateScore', listOfPlayers[socket.username].score, socket.username);
                    listOfPlayers[socket.username].listOfBullets.splice(i, 1);
                    listOfPedoBears.splice(pedoBear, 1);
                    len--;
                    i--;
                }
            }
        }
        io.sockets.emit('drawBullet',listOfPlayers[socket.username].listOfBullets,socket.username);
    });

    socket.on('revive', function(data){
        listOfPlayers[data].vivant = true;
        io.sockets.emit('updateplayers', listOfPlayers);
    });

    socket.on('beginGame', function(){
        io.sockets.emit('runGame');
    });

    socket.on('createPedoBear', function (){
        listOfPedoBears.push(new PedoBear());
    });

    function isEveryoneDead(){
        for(var player in listOfPlayers){
            if(listOfPlayers[player].vivant){
                return false;
            }
        }
        return true;
    }

    socket.on('updatePedoBear', function (){
        for(var pedoBear in listOfPedoBears) {
            listOfPedoBears[pedoBear].update(listOfPlayers);
        }
        io.sockets.emit('drawPedoBear',listOfPedoBears);
        for(var pedoBear in listOfPedoBears){
            for(player in listOfPlayers){
                if(circRectsOverlap(
                        listOfPedoBears[pedoBear].posX,
                        listOfPedoBears[pedoBear].posY,
                        20,
                        40,
                        listOfPlayers[player].posX,
                        listOfPlayers[player].posY,
                        7)){
                    // joueur est mort
                    listOfPlayers[player].vivant = false;
                    io.sockets.emit('updatePlayerDead', listOfPlayers[player].name);
                }
            }
        }
        if(isEveryoneDead()){
            io.sockets.emit('updateEtatCourant', "gameOver");
            io.sockets.emit('updateRecap', listOfPlayers);
            listOfPedoBears = [];
            for(var joueur in listOfPlayers) {
                listOfPlayers[joueur].listOfBullets = [];
            }
            io.sockets.emit('updateplayers', listOfPlayers);
        }
    });

    socket.on('revive', function(data){
        listOfPlayers[data].vivant = true;
        listOfPlayers[data].score = 0;
        listOfPlayers[data].posX = 400;
        listOfPlayers[data].posY = 300;
        listOfPlayers[data].vx = 0;
        listOfPlayers[data].vy = 0;
        listOfPlayers[data].pedobearsKilled = 0;
        listOfPlayers[data].listOfBullets = [];
        io.sockets.emit('updateplayers', listOfPlayers);
    });

    // when the client emits 'adduser', this listens and executes
    socket.on('adduser', function(joueur){
        // we store the username in the socket session for this client
        socket.username = joueur;
        var nbUsers = Object.keys(listOfPlayers).length % 4;
        switch (nbUsers) {
            case 0:
                listOfPlayers[joueur] = new Joueur(joueur, 'red');
                listOfPlayers[joueur].skinUrl = 'pics/characters/skin_red.png';
                break;
            case 1:
                listOfPlayers[joueur] = new Joueur(joueur, 'yellow');
                listOfPlayers[joueur].skinUrl = 'pics/characters/skin_yellow.png';
                break;
            case 2:
                listOfPlayers[joueur] = new Joueur(joueur, 'green');
                listOfPlayers[joueur].skinUrl = 'pics/characters/skin_green.png';
                break;
            case 3:
                listOfPlayers[joueur] = new Joueur(joueur, 'rgb(200,0,140)');
                listOfPlayers[joueur].skinUrl = 'pics/characters/skin_pink.png';
                break;
        }
        io.sockets.emit('updateplayers',listOfPlayers);
    });

    socket.on('manageInput', function(input){
        listOfPlayers[socket.username].update(input);
        io.sockets.emit('updateOnePlayerPosition',listOfPlayers[socket.username]);
    });

    socket.on('beginGame', function(){
        io.sockets.emit('runGame');
    });

     // when the user disconnects.. perform this
     socket.on('disconnect', function(){
        delete listOfPlayers[socket.username];
        io.sockets.emit('updatePlayers',listOfPlayers);
     });
});