// CLIENT \\

var canvas, ctx, w, h; // ctx : context
var smokeVideo;
var inputStates = {};
var allPlayers = {};
var allBonusScore = [];
var allPedoBear = [];
var currentTick = 0;
var xClicked = 0;
var yClicked = 0;
var socket = io.connect();
var username = prompt("what's your name?");
var mousePos = {x: 0, y: 0};
var backgroundFoot = new Image();
backgroundFoot.src = 'pics/background/foot_sombre.jpg';
var backgroundMist = new Image();
backgroundMist.src = 'pics/background/mist.jpg';
var bonusX2 = new Image();
bonusX2.src = 'pics/bonus/bonus_score_x2.png';
var bonusX3 = new Image();
bonusX3.src = 'pics/bonus/bonus_score_x3.png';
var bonusX5 = new Image();
bonusX5.src = 'pics/bonus/bonus_score_x5.png';
var pedobearFix = new Image();
pedobearFix.src = 'http://img11.hostingpics.net/pics/567630pedobearfix.png';
var skinRed = new Image();
var skinYellow = new Image();
var skinGreen = new Image();
var skinPink = new Image();
var pedobearLogo = new Image();
pedobearLogo.src = 'pics/pedobear_attack.png';
var titreJeu = new Image();
titreJeu.src = 'pics/titre_jeu.png';
var insideMenuItem = false;
var scoreFinal = 0;
var pedobearsKilledFinal = 0;
var listOfPlayersRecap = {};
var bulletBlack = new Image();
bulletBlack.src = 'pics/bullets/bullet_black.png';
var timeDebutPartie = 0;
var finalTime = 0;
var currentTime = 0;
var theTime = 0;
var intervalleEntreDeuxTirs = 40;
var intervalleEntreDeuxPedobears = 50;
var rect = 0;
var etats = {
    menuPrincipal: 0,
    jeuEnCours: 1,
    gameOver: 2,
    enAttente: 3
};
var enJeu = false;
var spectateur = false;

var etatCourant = etats.menuPrincipal;

var currSong = "";

window.onload = function () {
    document.oncontextmenu = new Function("return false");
    console.log("page chargée");
    canvas = document.querySelector("#myCanvas");
    ctx = canvas.getContext('2d');
    w = canvas.width;
    h = canvas.height;
    rect = canvas.getBoundingClientRect();
    // Définition des écouteurs
    document.addEventListener("keydown", traiteToucheAppuyee, false);
    document.addEventListener("keyup", traiteToucheRelevee, false);
    document.addEventListener("mousemove", mouseMoveListener, false);
    document.addEventListener("mousedown", mouseDownListener, false);

    anime();
};

function mouseDownListener(evt) {
    xClicked = evt.clientX - rect.left;
    yClicked = evt.clientY - rect.top;
}

// on connection to server, ask for user's name with an anonymous callback
socket.on('connect', function () {
    console.log("connected !");
    socket.emit('adduser', username);

    playSong("instruMenu");
    // On met 3 bonus quand la partie commence, juste pour tester
    var bonusInfo = {"multiplicateur": 5, "x": Math.floor(Math.random() * 801), "y": Math.floor(Math.random() * 601)};
    allBonusScore.push(bonusInfo);
    bonusInfo = {"multiplicateur": 3, "x": Math.floor(Math.random() * 801), "y": Math.floor(Math.random() * 601)};
    allBonusScore.push(bonusInfo);
    bonusInfo = {"multiplicateur": 2, "x": Math.floor(Math.random() * 801), "y": Math.floor(Math.random() * 601)};
    allBonusScore.push(bonusInfo);
});

function isRectangleClicked(x, y, w, h, mouseX, mouseY) {
    if ((mouseX > (x + w)) || (mouseX < x)) {
        return false;
    }
    if ((mouseY > (y + h)) || (mouseY < y)) {
        return false;
    }
    return true;
}

function playSong(newSong) {
    if (newSong != "") {
        if (currSong != "") {
            document.getElementById(currSong).pause();
        }
        var newSongLoad = document.getElementById(newSong);
        newSongLoad.currentTime = 0;
        newSongLoad.loop = true;
        newSongLoad.play();
        currSong = newSong;
    }
}

function playFireSong(fireSong) {
    var newSongLoad = document.getElementById(fireSong);
    newSongLoad.currentTime = 0;
    newSongLoad.play();
}

function playUpgradeSong() {
    var newSongLoad = document.getElementById("fxUpgrade");
    newSongLoad.currentTime = 0;
    newSongLoad.play();
}

// listener, whenever the server emits 'updatePos', this updates the chat body
socket.on('updatemousepos', function (data) {
    allPlayers[data.user].mousePosX = data.mousePos.x;
    allPlayers[data.user].mousePosY = data.mousePos.y;
});

socket.on('updateScore', function (newScore, name) {
    allPlayers[name].score = newScore;
    $("#users").empty();
    for (var player in allPlayers) {
        $("#users").append("<div id=\"nameAndScore\">" + allPlayers[player].name + " : " + allPlayers[player].score);
    }

});

socket.on('updateRecap', function (data) {
    listOfPlayersRecap = data;
    scoreFinal = listOfPlayersRecap[username].score;
    pedobearsKilledFinal = listOfPlayersRecap[username].pedobearsKilled;
});

socket.on('updatePlayerDead', function (name) {
    allPlayers[name].vivant = false;
});

socket.on('updateEtatCourant', function (etat) {
    switch (etat) {
        case "gameOver":
            if (enJeu || spectateur) {
                etatCourant = etats.gameOver;
                finalTime = currentTime;
                playSong("instruAttenteAutresJoueurs");
                enJeu = false;
            }
            break;
    }
});


socket.on('drawBullet', function (bullets, name) {
    allPlayers[name].listOfBullets = [];
    for (var i in bullets) {
        if (bullets[i].x != null) {
            allPlayers[name].listOfBullets[i] = bullets[i];
        }
    }
});

socket.on('drawPedoBear', function (pedoBears) {
    allPedoBear = [];
    for (var pedoBear in pedoBears) {
        allPedoBear.push(pedoBears[pedoBear]);
    }
});

// update the whole list of players, useful when a player
// connects or disconnects, we must update the whole list
socket.on('updateplayers', function (listOfplayers) {
    updatePlayers(listOfplayers);
});

socket.on('updateOnePlayerPosition', function (player) {
    allPlayers[player.name].posX = player.posX;
    allPlayers[player.name].posY = player.posY;

});

socket.on('runGame', function () {
    if (etatCourant == etats.enAttente) {
        etatCourant = etats.jeuEnCours;
        enJeu = true;
        spectateur = false;
        ctx.beginPath();
        ctx.arc(1000, 1000, 1, 0, 2 * Math.PI, false);
        ctx.fill();
        timeDebutPartie = theTime;
        /*for (var player in allPlayers) {
            if (allPlayers[player].pedobearsKilled > 0) {
                canPlay = false;
                break;
            }
        }*/
        playSong("instruIngame");
    }
});

function getMousePos(evt) {
    if (username !== 'undefined') {
        // necessary to take into account CSS boudaries
        console.log(username);
        allPlayers[username].mousePosX = evt.clientX - rect.left;
        allPlayers[username].mousePosY = evt.clientY - rect.top;
        return {
            x: evt.clientX - rect.left,
            y: evt.clientY - rect.top
        };
    }
}

function mouseMoveListener(evt) {

    mousePos = getMousePos(evt);
    var data = {
        user: username,
        mousePos: mousePos
    };
    socket.emit('sendnewmousepos', data);
}

function traiteToucheAppuyee(evt) {
    if (evt.keyCode === 81) {
        inputStates.left = true;
    } else if (evt.keyCode === 90) {
        inputStates.up = true;
    } else if (evt.keyCode === 68) {
        inputStates.right = true;
    } else if (evt.keyCode === 83) {
        inputStates.down = true;
    } else if (evt.keyCode === 32) {
        inputStates.space = true;
    }
}

function traiteToucheRelevee(evt) {
    if (evt.keyCode === 81) {
        inputStates.left = false;
    } else if (evt.keyCode === 90) {
        inputStates.up = false;
    } else if (evt.keyCode === 68) {
        inputStates.right = false;
    } else if (evt.keyCode === 83) {
        inputStates.down = false;
    } else if (evt.keyCode === 32) {
        inputStates.space = false;
    }
}

// Mise à jour du tableau quand un joueur arrive
// ou se deconnecte
function updatePlayers(listOfPlayers) {
    allPlayers = listOfPlayers;
    $("#users").empty();
    for (var player in allPlayers) {
        $("#users").append("<div id=\"nameAndScore\">" + allPlayers[player].name + " : " + allPlayers[player].score);
    }
}

function drawPlayer(player) {
    ctx.save();
    ctx.translate(player.posX, player.posY);
    ctx.shadowColor = "black"; // color
    ctx.shadowBlur = 10; // blur level
    ctx.shadowOffsetX = 5; // horizontal offset
    ctx.shadowOffsetY = 5; // vertical offset
    var dx = player.mousePosX - player.posX;
    var dy = player.mousePosY - player.posY;
    var theta = Math.atan(dy / dx);
    var reverse = 0;
    if (dx < 0) {
        reverse = Math.PI;
    }

    ctx.rotate(theta + reverse);

    if (player.color == "red") {
        skinRed.src = player.skinUrl;
        ctx.drawImage(skinRed, -24, -16, 48, 32);
    } else if (player.color == "yellow") {
        skinYellow.src = player.skinUrl;
        ctx.drawImage(skinYellow, -24, -16, 48, 32);
    } else if (player.color == "green") {
        skinGreen.src = player.skinUrl;
        ctx.drawImage(skinGreen, -24, -16, 48, 32);
    } else if (player.color == "rgb(200,0,140)") {
        skinPink.src = player.skinUrl;
        ctx.drawImage(skinPink, -24, -16, 48, 32);
    }

    ctx.restore();
}

function drawAllPlayers() {
    for (var name in allPlayers) {
        if (allPlayers[name].vivant) {
            drawPlayer(allPlayers[name]);
        }
    }
}

function drawAllScope() {
    for (var name in allPlayers) {
        if (allPlayers[name].vivant) {
            drawScope(allPlayers[name]);
        }
    }
}


/** Style du viseur **/
function drawScope(player) {
    ctx.save();
    ctx.translate(player.mousePosX, player.mousePosY);

    rectLarg = 2;
    rectLong = 10;
    ctx.fillStyle = player.color;
    ctx.fillRect(0 - rectLarg / 2, 0 - rectLong / 2, rectLarg, rectLong);
    ctx.fillRect(0 - rectLong / 2, 0 - rectLarg / 2, rectLong, rectLarg);
    ctx.fill();

    ctx.restore();
}

function manageInput() {
    if (allPlayers[username].vivant) {
        socket.emit('manageInput', inputStates);
    }
}

function createBullet() {
    if (currentTick % intervalleEntreDeuxTirs == 0 && (allPlayers[username].vivant)) {
        playFireSong("magnumModif");
        socket.emit('createBullet');
    }
}

function updateBullet() {
    socket.emit('updateBullet');
}
function drawBullet(bullets) {
    for (var i in bullets) {
        ctx.drawImage(bulletBlack, bullets[i].x - 5, bullets[i].y - 7, 12, 12);
    }
}

function drawAllBullet() {
    for (var i in allPlayers) {
        ctx.strokeStyle = allPlayers[i].color;
        drawBullet(allPlayers[i].listOfBullets);
    }
}

function createPedoBear() {
    if (currentTick % intervalleEntreDeuxPedobears == 0) {
        socket.emit('createPedoBear');
    }
}

function updatePedoBear() {
    socket.emit('updatePedoBear');
}

function drawPedoBear() {
    for (pedoBear in allPedoBear) {
        ctx.save();
        ctx.translate(allPedoBear[pedoBear].posX, allPedoBear[pedoBear].posY);
        ctx.shadowColor = "black"; // color
        ctx.shadowBlur = 10; // blur level
        ctx.shadowOffsetX = 5; // horizontal offset
        ctx.shadowOffsetY = 5; // vertical offset
        ctx.drawImage(pedobearFix, 0, 0, 20, 40);

        ctx.restore();
    }
}


function drawBackground() {
    smokeVideo = document.getElementById('smoke');
    ctx.save();
    ctx.drawImage(smokeVideo, 0, 0, w, h);
    ctx.restore();
}

function drawMenu() {
    ctx.fillStyle = "white";
    ctx.drawImage(pedobearLogo, 705, 280, 48, 48);

    ctx.drawImage(titreJeu, 250, 10, 300, 60);
    ctx.font = "25px Georgia";
    if (isRectangleClicked(w - 295, 275, 55, 22, mousePos.x, mousePos.y)) {
        ctx.font = "28px Georgia";
    }
    ctx.fillText("Play", w - 295, 295);

    ctx.font = "15px Georgia";
    ctx.fillText("Developed By Zied Benzarti & Marc Destefanis", w - 320, 595);

    ctx.beginPath();
    ctx.strokeStyle = "white";
    ctx.lineWidth = 1;
    ctx.moveTo(canvas.width - 325, 225);
    ctx.lineTo(canvas.width - 325 + Math.cos(currentTick / 10), 325);
    ctx.moveTo(canvas.width - 325, 325 + Math.cos(currentTick / 10));
    ctx.lineTo(750, 325);
    ctx.stroke();

    var radius = 10;
    ctx.beginPath();
    ctx.arc(canvas.width - 325 + Math.cos(currentTick / 10) * 3, 325 + Math.cos(currentTick / 10) * 3, radius, 0, 2 * Math.PI, false);
    ctx.fill();
    ctx.stroke();

    radius = 2;
    ctx.arc(w - 165 + Math.cos(currentTick / 100) * 155, 598, radius, 0, 2 * Math.PI, false);
    ctx.fill();
}

function manageFxHoverSong() {
    // manage hover FX reload song
    if (isRectangleClicked(w - 295, 275, 55, 22, mousePos.x, mousePos.y) && !insideMenuItem) {
        playFireSong("fxMenuReload");
        insideMenuItem = true;
    } else if (!isRectangleClicked(w - 295, 275, 55, 22, mousePos.x, mousePos.y)) {
        insideMenuItem = false;
    }
}

function manageClick(time) {
    if (isRectangleClicked(w - 295, 275, 55, 22, xClicked, yClicked)) {

        // Play
        playFireSong("fxMenuFire");
        xClicked = 1000;

        var radius = 0;
        playSong("instruAttenteAutresJoueurs");
        ctx.beginPath();
        ctx.arc(1000, 1000, radius, 0, 2 * Math.PI, false);
        ctx.fill();
        ctx.lineWidth = 2;
        ctx.stroke();
        intervalleEntreDeuxPedobears = 50;
        intervalleEntreDeuxTirs = 40;
        var canPlay = true;

        if(canPlay){
            etatCourant = etats.enAttente;
            socket.emit('revive', username);
        }
        else {
            spectateur = true;
            etatCourant = etats.jeuEnCours;
        }
    }
}

function manageDifficulty(t) {
    if (t < 10) {
        intervalleEntreDeuxPedobears = 50;
    } else if (t < 20) {
        intervalleEntreDeuxPedobears = 40;
    } else if (t < 30) {
        intervalleEntreDeuxPedobears = 30;
    } else if (t < 40) {
        intervalleEntreDeuxPedobears = 20;
    } else if (t < 50) {
        intervalleEntreDeuxPedobears = 15;
    } else if (t < 60) {
        intervalleEntreDeuxPedobears = 12;
    } else if (t < 70) {
        intervalleEntreDeuxPedobears = 10;
    } else if (t < 80) {
        intervalleEntreDeuxPedobears = 9;
    } else if (t < 90) {
        intervalleEntreDeuxPedobears = 8;
    } else if (t < 100) {
        intervalleEntreDeuxPedobears = 7;
    } else if (t < 110) {
        intervalleEntreDeuxPedobears = 6;
    } else if (t < 120) {
        intervalleEntreDeuxPedobears = 5;
    } else if (t < 130) {
        intervalleEntreDeuxPedobears = 4;
    } else if (t < 140) {
        intervalleEntreDeuxPedobears = 3;
    } else if (t < 150) {
        intervalleEntreDeuxPedobears = 2;
    } else if (t < 160) {
        intervalleEntreDeuxPedobears = 1;
    }
}

function manageVitesseTir(sc) {
    switch (sc) {
        case 1000 :
            if (intervalleEntreDeuxTirs != 30) {
                intervalleEntreDeuxTirs = 30;
                playUpgradeSong();
            }
            break;
        case 3000 :
            if (intervalleEntreDeuxTirs != 20) {
                intervalleEntreDeuxTirs = 20;
                playUpgradeSong();
            }
            break;
        case 6000 :
            if (intervalleEntreDeuxTirs != 15) {
                intervalleEntreDeuxTirs = 15;
                playUpgradeSong();
            }
            break;
        case 10000 :
            if (intervalleEntreDeuxTirs != 10) {
                intervalleEntreDeuxTirs = 10;
                playUpgradeSong();
            }
            break;
        case 15000 :
            if (intervalleEntreDeuxTirs != 9) {
                intervalleEntreDeuxTirs = 9;
                playUpgradeSong();
            }
            break;
        case 21000 :
            if (intervalleEntreDeuxTirs != 8) {
                intervalleEntreDeuxTirs = 8;
                playUpgradeSong();
            }
            break;
        case 30000 :
            if (intervalleEntreDeuxTirs != 7) {
                intervalleEntreDeuxTirs = 7;
                playUpgradeSong();
            }
            break;
    }
}

function timePrinter(timeInput) {
    var minutes = Math.floor(timeInput / 60);
    var secondes = timeInput % 60;
    return minutes + "\"" + secondes;
}

function drawScore() {
    var nbPlayers = Object.keys(listOfPlayersRecap).length;
    var position = 1;
    for (joueur in listOfPlayersRecap) {
        if (listOfPlayersRecap[joueur].score > listOfPlayersRecap[username].score) {
            position++;
        }
    }
    ctx.fillStyle = "white";

    ctx.drawImage(titreJeu, 250, 10, 300, 60);

    // Pseudo du joueur 1
    ctx.font = "25px Georgia";
    ctx.fillText(allPlayers[username].name, 375, 150);

    ctx.font = "20px Georgia";
    ctx.fillText("Score", 100, 200);
    ctx.fillText("Pedobears Killed", 100, 250);
    ctx.fillText("Lifetime", 100, 300);
    ctx.fillText("Rank", 100, 350);

    // Score
    ctx.fillText(scoreFinal, 375, 200);

    // Pedobears killed
    ctx.fillText(pedobearsKilledFinal, 375, 250);

    // Lifetime
    ctx.fillText(timePrinter(finalTime), 375, 300);

    // Rank
    ctx.fillText(position + "/" + nbPlayers, 375, 350);

    // Press space to continue qui defile
    ctx.font = 17 + "px Georgia";
    ctx.fillText("Press space to continue", -210 + ((currentTick) % 1080), 595);

    // Cercle en dessous de 'press space to continue' qui defile
    var radius = 2;
    ctx.beginPath();
    ctx.arc(Math.cos(currentTick / 100) * 85 - 123 + ((currentTick) % 1080), 598, radius, 0, 2 * Math.PI, false);
    ctx.fill();
}

function drawEnAttenteAnim() {
    ctx.fillStyle = "white";

    ctx.drawImage(titreJeu, 250, 10, 300, 60);

    ctx.font = 21 + "px Georgia";
    ctx.fillText("Waiting for other players", 285, 250);

    // Press space to continue qui defile
    ctx.font = 17 + "px Georgia";
    ctx.fillText("Press space to continue", -210 + ((currentTick) % 1080), 595);

    // Cercle en dessous de 'press space to continue' qui defile
    var radius = 2;
    ctx.beginPath();
    ctx.arc(400 + Math.cos(currentTick / 10) * 20, 300 + Math.sin(currentTick / 10) * 20, radius, 0, 2 * Math.PI, false);
    ctx.arc(Math.cos(currentTick / 100) * 85 - 123 + ((currentTick) % 1080), 598, radius, 0, 2 * Math.PI, false);
    ctx.fill();
}

function manageSpaceInput() {
    if (inputStates.space && etatCourant == etats.gameOver) {
        etatCourant = etats.menuPrincipal;
        xClicked = 1000;
        yClicked = 1000;
        playSong("instruMenu");
        enJeu = true;
        spectateur = false;
        for (var player in allPlayers) {
            allPlayers[player].pedobearsKilled = 0;
            allPlayers[player].score = 0;
            scoreFinal = 0;
        }
    } else if (inputStates.space && etatCourant == etats.enAttente) {
        socket.emit('beginGame');
    }
}

function anime(time) {
    theTime = time;
    if (allPlayers[username] !== undefined) {
        // 1 On efface l'écran
        ctx.clearRect(0, 0, w, h);

        switch (etatCourant) {
            case etats.menuPrincipal:
                drawBackground();
                drawMenu();
                drawScope(allPlayers[username]);
                manageClick(time);
                manageFxHoverSong();
                currentTick++;
                spectateur = false;
                enJeu = false;
                break;
            case etats.jeuEnCours:
                currentTime = ((time - timeDebutPartie) / 1000).toFixed(0);
                $('#chrono').text(timePrinter(currentTime));
                ctx.globalAlpha = 1;
                ctx.drawImage(backgroundFoot, 0, 0, w, h);
                ctx.globalAlpha = 0.1;
                ctx.drawImage(backgroundMist, 0, 0, w, h);
                ctx.globalAlpha = 0.9; // opacité sur les joueurs, scopes, etc
                manageInput();
                manageVitesseTir(allPlayers[username].score);
                createBullet();
                updateBullet();
                drawAllBullet();
                manageDifficulty(currentTime);
                createPedoBear();
                updatePedoBear();
                drawPedoBear();
                drawAllPlayers();   // Dessine les joueurs
                drawAllScope();     // Dessine le viseur des joueurs
                currentTick++;
                break;
            case etats.gameOver:
                // afficher resultats de la partie, click pour retour au menu principal
                ctx.globalAlpha = 1;
                ctx.fillStyle = "black";
                ctx.fillRect(0, 0, w, h);
                drawScore();
                drawScope(allPlayers[username]);
                manageSpaceInput();
                currentTick++;
                break;
            case etats.enAttente:
                ctx.globalAlpha = 1;
                ctx.fillStyle = "black";
                ctx.fillRect(0, 0, w, h);
                manageSpaceInput();
                drawEnAttenteAnim();
                drawScope(allPlayers[username]);
                currentTick++;
                break;
        }
    }
    // 4 On rappelle la fonction d'animation à 60 im/s
    requestAnimationFrame(anime);
}
